;hello.asm

section .text
	global _start
_start:
	jmp 	short one
	
two:
	pop		ecx

	xor		eax, eax
	mov		al, 4
	xor		ebx, ebx
	inc		ebx
	xor		edx, edx
	mov		dl, 10    ; This length counts "0x0d" (newline) as well
	int		0x80
	
	xor 	eax, eax
	mov		al, 1
	xor		ebx, ebx
	int		0x80
	
one:
	call two
	db "Hello_ASM", 0x0a
