#include <stdio.h>

int isValidAdd(signed int a, signed int b){
	if(a >0 && b>0){
		return !(a+b < a);
	}
	if(a<0 && b<0){
		return !(a+b>a);
	}
	return 1;
}

int main(){
	int result = isValidAdd(-2100000000, -2100000000);
	printf("result: %u\n", result);
	return 1;
}
