#include <iostream>
#include <vector>
#include <thread>
using namespace std;

int accum = 0;
void square(int x){
	int temp = accum;
	temp += x*x;
	accum = temp;
}

int main(){
	vector <thread> ths;
	for(int i =1; i<=150; i++){
		ths.push_back(thread(&square, i));
	}
	for (auto& th : ths){
		th.join();
	}
	cout << "accum = " << accum << endl;
	return 0;
}
