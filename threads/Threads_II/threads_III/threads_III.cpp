#include <iostream>
#include <thread>
#include <vector>

using namespace std; int y;

void func(int x){
	cout << "Inside thread " << x << endl;
	y = x;
	printf("y: %d", y);
}

int main(){
	vector <thread> thrs;
	for(int i; i<=1000; i++)
		thrs.push_back(thread(&func, i));
	for(auto& th : thrs)
		th.join();
	cout << "Outside thread" << endl;
	return 0;
}
